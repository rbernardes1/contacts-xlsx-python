from openpyxl import load_workbook
from functions_xlsx import read_spread_sheet
#path = '\Define\your\path\contacts.xlsx'

excel_file = load_workbook('contacts.xlsx')
#excel_file = load_workbook(path)

spread_sheet1 = excel_file.active

spread_sheet1.title = 'contacts'

spread_sheet2 = excel_file.create_sheet('monthly_expenses')

print(excel_file.sheetnames)

spread_sheet2['A1'] = 'Category'
spread_sheet2['B1'] = 'Amount'

value_list = [
    ('restaurant',35.44),
    ('drug store',10.99),
    ('supermarket', 435.89),
    ('sum','=SOMA(B2:B4)')
    ]

for row in value_list:
    spread_sheet2.append(row)


read_spread_sheet(spread_sheet2)

excel_file.save('contacts_monthly_expenses.xlsx')