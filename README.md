# contacts-xlsx-python

## Description

This project manipulates a .xlsx file using `Python`.

## What is necessary to run this project
To run this project it is necessary to have `Python`, `Git` and lib `openpyxl`.

- The download of Git is available on this [link](https://git-scm.com/downloads).

- The download of Python is available on this [link](https://www.python.org/downloads/). 

- To access the project and clone through `cmd`

`git clone https://gitlab.com/rbernardes1/search-algorithms.git`

- To install Openpyxl lib :

`pip install openpyxl`

## Authors
- [**Renata Bernardes**](https://gitlab.com/rbernardes1)
